﻿using SecretCrush.Data;

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Graphics.Display;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Parse;
using Newtonsoft.Json;
using Windows.Devices.Geolocation;
using Windows.UI.ApplicationSettings;
using Windows.UI.Popups;

namespace SecretCrush
{
    public sealed partial class ItemsPage : SecretCrush.Common.LayoutAwarePage
    {
        Geolocator geoPt = null;
        Geoposition geoPos = null;

        public ItemsPage()
        {
            this.InitializeComponent();
        }

        protected override void LoadState(Object navigationParameter, Dictionary<String, Object> pageState)
        {
            SettingsPane.GetForCurrentView().CommandsRequested += ShowPrivacyPolicy;
            geoPt = new Geolocator();
            findCrushes();
        }

        // PRIVACY POLICY
        // Method to add the privacy policy to the settings charm
        private void ShowPrivacyPolicy(SettingsPane sender, SettingsPaneCommandsRequestedEventArgs args)
        {
            SettingsCommand privacyPolicyCommand = new SettingsCommand("privacyPolicy", "Privacy Policy", (uiCommand) => { LaunchPrivacyPolicyUrl(); });
            args.Request.ApplicationCommands.Add(privacyPolicyCommand);
        }

        // Method to launch the url of the privacy policy
        async void LaunchPrivacyPolicyUrl()
        {
            Uri privacyPolicyUrl = new Uri("http://grantgumina.com/secretcrush/privacypolicy.html");
            var result = await Windows.System.Launcher.LaunchUriAsync(privacyPolicyUrl);
        }

        private async void findCrushes()
        {
            try
            {
                // get current location
                geoPos = await geoPt.GetGeopositionAsync();
                ParseGeoPoint pGeoPt = new ParseGeoPoint(geoPos.Coordinate.Latitude, geoPos.Coordinate.Longitude);

                // create a bounding box
                double northEastCornerLat = pGeoPt.Latitude + changeInLatitude(5);
                double southWestCornerLat = pGeoPt.Latitude - changeInLatitude(5);
                double northEastCornerLong = pGeoPt.Longitude + changeInLongitude(pGeoPt.Latitude, 5);
                double southWestCornerLong = pGeoPt.Longitude - changeInLongitude(pGeoPt.Latitude, 5);
                ParseGeoPoint northEastCorner = new ParseGeoPoint(northEastCornerLat, northEastCornerLong);
                ParseGeoPoint southWestCorner = new ParseGeoPoint(southWestCornerLat, southWestCornerLong);

                var query = from crush in ParseObject.GetQuery("SecretCrush")
                            .WhereWithinGeoBox("location", southWestCorner, northEastCorner)
                            orderby crush.CreatedAt descending
                            select crush;

                crushesListView.Items.Clear();

                IEnumerable<ParseObject> crushes = await query.FindAsync();
                foreach (ParseObject crush in crushes)
                {
                    crushesListView.Items.Add(crush["description"]);
                }
            }
            catch
            {
                
            }
        }

        private double changeInLatitude(double miles)
        {
            return ((miles / 3960) * (180 / Math.PI));
        }

        private double changeInLongitude(double latitude, double miles)
        {
            double x = 3960 * Math.Cos(latitude * (Math.PI / 180));
            return ((miles / x) * (180 / Math.PI));
        }

        private async void submitBtn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                // check if text is valid
                if (crushTxt.Text != "" && crushTxt.Text != "Who's your secret crush?")
                {
                    ParseObject newCrush = new ParseObject("SecretCrush");
                    newCrush["description"] = crushTxt.Text;
                    newCrush["location"] = new ParseGeoPoint(geoPos.Coordinate.Latitude, geoPos.Coordinate.Longitude);
                    await newCrush.SaveAsync();
                    findCrushes();

                    crushTxt.Text = "Who's your secret crush?";
                    crushTxt.SelectionStart = 0;
                    crushTxt.SelectionLength = crushTxt.Text.Length;
                }
            }
            catch
            {
            }
        }

        private void crushTxt_GotFocus(object sender, RoutedEventArgs e)
        {
            crushTxt.SelectionStart = 0;
            crushTxt.SelectionLength = crushTxt.Text.Length;
        }

        private void crushesListView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                int ind = crushesListView.SelectedIndex;
                string desc = crushesListView.Items[ind].ToString();
                this.Frame.Navigate(typeof(CrushDetail), desc);
            }
            catch 
            {
            }
        }

        private void StackPanel_Tapped(object sender, TappedRoutedEventArgs e)
        {
            try
            {
                findCrushes();
            }
            catch
            {}
        }
    }
}
